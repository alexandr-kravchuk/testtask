<?php

function get_numeric(array $array): array
{
    return array_reduce($array, function ($isNumeric, $item) {

        if (is_numeric($item)) $isNumeric[] = $item;

        return $isNumeric;
    });
}

$numbers = "38,42,58,48,33,59,87,17,20,8,98,14,62,66,14,62,97,66,74,78,66,2,79,29,72,6,3,71,46,68,48 ,4,12,52,66,48,14,39,63,69,81,61,21,77,10,44,39,82,19,77,100,98,53,95,30,17,30,96,68,47, 81,52,82,11,13,83,10,14,49,96,27,73,42,76,71,15,81,36,77,38,17,2,29,100,26,86,22,18,38,6 4,82,51,39,7,88,53,82,30,98,86";
$li_of_numbers = get_numeric(explode(',', $numbers));

echo '<pre>';
print_r($li_of_numbers);
echo '</pre>';