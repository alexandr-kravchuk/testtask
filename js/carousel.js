let i = 0
let images = []
let time = 3000
let isEnabledAutoScroll = false
let t = ''

images[0] = "img/1.jpg";
images[1] = "img/2.jpg";
images[2] = "img/3.jpg";
images[3] = "img/4.jpg";
images[4] = "img/5.jpg";
images[5] = "img/6.jpg";
images[6] = "img/7.jpg";
images[7] = "img/8.jpg";
images[8] = "img/9.jpg";

function setAutoScroll() {
    if (isEnabledAutoScroll) {
        isEnabledAutoScroll = false
        clearInterval(t)
    } else {
        isEnabledAutoScroll = true
        t = setInterval("changeImg(i)", time);
    }
}

function changeImg(current) {
    let length = images.length

    if (current === -1) {
        i--
        if (i === -1) {
            i = length - 1
        }
    } else {
        i++
        if (i > length - 1) {
            i = 0
        }
    }

    document.slide.src = images[i];
}

window.onload = changeImg;

function showModal() {
    let modal = document.getElementById("myModal");
    let span = document.getElementsByClassName("close")[0];
    modal.style.display = "block";
    let currentImage = document.getElementById('currentImage')
    currentImage.src = images[i]

    span.onclick = function () {
        modal.style.display = "none";
    }

    window.onclick = function (event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    }
}

setAutoScroll()